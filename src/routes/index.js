import Home from '../views/Home';
import Group from '../views/Group'
import Project from '../views/Project'
import Module from '../views/Module'

export default [{
  name: 'Home',
  route: '/home',
  component: Home,
  exact: true,
  children: [{
    name: 'Module',
    parent: '/home',
    route: '/module',
    component: Module,
  }]
}, {
  name: 'Group',
  route: '/group',
  exact: true,
  component: Group,
}, {
  name: 'Project',
  route: '/project',
  exact: true,
  component: Project,

},]