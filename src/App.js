import React from 'react';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom'

import AppLayout from './container/AppLayout';
import Login from './views/Auth/Login';
import Signup from './views/Auth/Signup'


const App = ({location}) => {
  return (
    <Switch>
      <Route path='/app' component={AppLayout} location={location} />
      <Route path='/login' component={Login} />
      <Route path='/signup' component={Signup} />
      <Redirect to="/app" />
    </Switch>
  );
}

export default withRouter(App);
