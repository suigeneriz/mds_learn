import React from "react";
import Card from "../components/Card";
import link from "../assets/images/link.png";
import path from "../assets/images/Path.png";

const Projects = () => (
  <div className="Projects">
    <div className="container-fluid">
      <div className="row justify-content-center">
        <div className="col-12 col-md-11">
          <Card className="body border Monserrat mb-2">
            <span className="project-week text-center">01</span>
            <div className="d-flex justify-content-around">
              <div>
                <p className="project1 mb-1">Design a Landing Page on Figma</p>
                <img src={link} alt="link" className="link" />
                <a href="_/_" className="mt-2">
                  Link
                </a>
              </div>
              <div className="d-flex">
                <p className="font-weight-bold">Completed</p>
                <img src={path} alt="path" className="path text-center ml-3" />
              </div>

              <div className="d-flex date-time">
                <p>01 Jan, 2020</p>
                <p className="ml-3">00:00</p>
              </div>
            </div>
          </Card>

          <Card className="body border Monserrat mb-2 mt-0">
            <span className="project-week text-center">01</span>
            <div className="d-flex justify-content-around">
              <div>
                <p className="project1 mb-1">Design a Landing Page on Figma</p>
                <img src={link} alt="link" className="link" />
                <a href="_/_" className="mt-2">
                  Link
                </a>
              </div>
              <div className="d-flex">
                <p className="font-weight-bold">Completed</p>
                <img src={path} alt="path" className="path text-center ml-3" />
              </div>

              <div className="d-flex date-time">
                <p>01 Jan, 2020</p>
                <p className="ml-3">00:00</p>
              </div>
            </div>
          </Card>

          <Card className="body border Monserrat mb-2 mt-0">
            <span className="project-week text-center">01</span>
            <div className="d-flex justify-content-around">
              <div>
                <p className="project1 mb-1">Design a Landing Page on Figma</p>
                <img src={link} alt="link" className="link" />
                <a href="_/_" className="mt-2">
                  Link
                </a>
              </div>
              <div className="d-flex">
                <p className="font-weight-bold">Completed</p>
                <img src={path} alt="path" className="path text-center ml-3" />
              </div>

              <div className="d-flex date-time">
                <p>01 Jan, 2020</p>
                <p className="ml-3">00:00</p>
              </div>
            </div>
          </Card>

          <Card className="body border Monserrat mb-2 mt-0">
            <span className="project-week text-center">01</span>
            <div className="d-flex justify-content-around">
              <div>
                <p className="project1 mb-1">Design a Landing Page on Figma</p>
                <img src={link} alt="link" className="link" />
                <a href="_/_" className="mt-2">
                  Link
                </a>
              </div>
              <div className="d-flex">
                <p className="font-weight-bold">Completed</p>
                <img src={path} alt="path" className="path text-center ml-3" />
              </div>

              <div className="d-flex date-time">
                <p>01 Jan, 2020</p>
                <p className="ml-3">00:00</p>
              </div>
            </div>
          </Card>

          <Card className="body border Monserrat mb-2 mt-0">
            <span className="project-week text-center">01</span>
            <div className="d-flex justify-content-around">
              <div>
                <p className="project1 mb-1">Design a Landing Page on Figma</p>
                <img src={link} alt="link" className="link" />
                <a href="_/_" className="mt-2">
                  Link
                </a>
              </div>
              <div className="d-flex">
                <p className="font-weight-bold">Completed</p>
                <img src={path} alt="path" className="path text-center ml-3" />
              </div>

              <div className="d-flex date-time">
                <p>01 Jan, 2020</p>
                <p className="ml-3">00:00</p>
              </div>
            </div>
          </Card>
        </div>
      </div>
    </div>
  </div>
);

export default Projects;
