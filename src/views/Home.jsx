
import React from "react";
import Card from "../components/Card";
import Progress from "../components/Progress";
import Icon from "../assets/images/Icons";
import Icons from "../assets/images/Icons";
import { history } from "../index";

const Home = () => (
  <div className="home">
    <div>
      <div className="col-md-7">
        <Card
          onClick={() => history.push("/app/home/module")}
          className="h-100">
        
          <div className="heading">Current Module</div>
          <Card className="body border">
            <div className="bookmark-container">
              <span className="bookmark">01</span>
              <span className="float-right">
                <Icons.Back fill="#FF3C3C" />
              </span>
            </div>
            <div className="heading-intro font-weight-bold">
              Introduction to Product Design/Figma
            </div>
          </Card>
          <Card className="body border">
            <div className="timeline">
              <div>
                <Icon.Clipboard /> Design a landing page
              </div>
              <span className="float-right">
                <Icons.Back fill="#FF3C3C" />
              </span>
            </div>
          </Card>
        </Card>
      </div>
      <div className="col-md-5 text-center">
        <Card className="h-100">
          <div className="heading">Course Progress</div>
          <div className="text-center">
            <Progress value={80} />
          </div>
          <div className="module-count">
            <p>26 Modules</p> <span className="circle" />
          </div>
        </Card>
      </div>
      <div className="col-md-12">
        <div className="heading modules pt-5">All Modules</div>
        {modules.map((module, index) => (
          <Card
            className="module-text border mb-5"
            as={module.status === "open" ? "a" : null}
            key={index}
            onClick="/app/home/module">
          
            <div className="content">
              <span className="title display-flex">
                <span className="count">{formatNumber(index + 1)}</span>
                {module.title}
              </span>
              <span className="float-right">
                {module.status === "open" ? <Icon.Back fill="#FF3C3C" /> : <Icon.Lock />}
              </span>
            </div>
          </Card>
        ))}
      </div>
    </div>
  </div>
);

const modules = [
  { title: "Introduction to figma", status: "locked" },

  { title: "Introduction to figma", status: "open" },

  { title: "Introduction to figma", status: "locked" }
];

const formatNumber = value => (value < 10 ? "0" + value : value);

export default Home;
