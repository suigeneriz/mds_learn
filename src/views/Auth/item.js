import React from "react";

const Item = props => {
  return (
    <div>
      {/* col-form-label is the class name of css content inside index.css, and is
      use for styling you code */}
      <label className="col-form-label ml-2">{props.label}</label>
      <input
        id="name"
        className="form-control"
        name={props.name}
        placeholder={props.placeholder}
        type={props.type}
        for={props.for}
        style={{
          width: '100%'
        }}
      />
    </div>
  );
};

export default Item;
