import React from "react";
import Item from "./item";
import signup from "../../assets/images/signup.svg";
import { history } from "../../index";

const Signup = () => {
  const submit = (e) => {
    e.preventDefault()
    history.push('/app/home')

  }
  return (
    <main className="login">
     
      
      <div className="container-fluid">
  
      <div className="row justify-content-md-between">
        <div className="col-12 col-md-6">

          <div className="img-fluid">
              <img src={signup} alt="signup" className="signup-img img-fluid" />
          </div>
        </div>
        
        <div className="col-12 col-md-6 auth-form">
          <form onSubmit={submit} className="item-label justify-content-md-center p-5" >
            <h2 class="login_text Eczar font-weight-bolder m-0 pb-4">Create Account </h2>

            <div className="pb-2">
            <Item
                 label="Your Fullname"
                 placeholder="Maria Joseph"
                 type="text"
                 name="name"
               />
               </div>

               <div className="pb-2">
               <Item
                 label="Phone Number"
                 placeholder="09057620692"
                 type="tel"
                 for="phone"
                 name="name"
               />
               </div>

               <div className="pb-2">
               <Item
                 label="Email Address"
                 placeholder="mariajoseph@gmail.com"
                 type="text"
                 name="email"
                 for="email"
               />
               </div>

               <div className="pb-2">
               <Item
                 label="State of Residence"
                 placeholder="Maria Joseph"
                 type="text"
                name="name"
               />
               </div>

               <div className="pb-2">
               <Item
                label="Password"
                 placeholder="*****"
                 type="password"
                name="password"
                 for="password"
               />
               </div>
            <button
              id="submitButton"
              type="submit"
              className="btn btn-danger btn-lg btn-block py-3 mt-4">
               SignUp
            </button>

          

            {/* <div class="card-footer justify-content-center">
              <small class="text-muted mt-sm-md-5">
                <small class="lead">&copy;</small> All right reserved.
                Motionwares Digital Solution Ltd.
              </small>
            </div> */}
          </form>
        </div>
      </div>
      </div>
    </main>
  );
};

export default Signup;
