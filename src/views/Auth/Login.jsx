import React from "react";
import Item from "./item";
import login from "../../assets/images/login.svg";
import { history } from "../../index";

const Login = () => {
  const submit = (e) => {
    e.preventDefault()
    history.push('/app/home')

  }
  return (
    <main className="login">
     
      
      <div className="container-fluid col-md-12">
      <a class="btn" href="_/_">
        <svg class="text-icon align-top">
          <use href="">#shadow-root (close)</use>
        </svg>
        <span>BACK</span>
      </a>

      <div className="row justify-content-md-between">
        <div className="col-12 col-md-6">

          <div className="img-container">
           
              <img src={login} alt="login" className="login-img img-fluid" />
            
          </div>
        </div>
        
        <div className="col-md-6 auth-form">
          <form onSubmit={submit} className="item-label justify-content-md-center p-1" >
            <h2 class="login_text Eczar font-weight-bolder m-0 pb-4"> Course Login </h2>

            <div className="pb-2">
            <Item
              label="Phone Number"
              placeholder="09057620692"
              type="tel"
              for="phone"
              name="name"
            />
          </div>

          <div className="pb-2">
            <Item
              label="Password"
              placeholder="*****"
              type="password"
              name="password"
              for="password"
            />
            </div>

            <button
              id="submitButton"
              type="submit"
              className="btn btn-danger btn-lg btn-block py-3 mt-4">
               Log In
            </button>

            <div className="d-flex justify-content-center mt-3">
            <p className="login_new Montserrat">New Here?...</p>
            <a className="login_new Montserrat pl-2" href="_/_">Create An Account</a>
            </div>

            {/* <div class="card-footer justify-content-center">
              <small class="text-muted mt-sm-md-5">
                <small class="lead">&copy;</small> All right reserved.
                Motionwares Digital Solution Ltd.
              </small>
            </div> */}
          </form>
        </div>
      </div>
      </div>
    </main>
  );
};

export default Login;
