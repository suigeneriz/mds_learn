import React from "react";
import ellipse from "../assets/images/Ellipse.png";
import path from "../assets/images/Path.png";
import Alt5 from "../assets/images/Alt 5.png";
import Vector30 from "../assets/images/Vector 30.png";

const Group = () => (
  <div className="container-fluid">
    <div className="row d-flex">
      <div className="col-12 col-md-5">
        <div className="d-flex pt-5">
          <img src={ellipse} alt="ellipse" className="ellipse" />

          <p className="heading-group Monserrat ml-5">Figma Basics</p>
        </div>
        <div className="pt-3">
          <p className="group-text text-justify">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat.
          </p>
        </div>

        <div className="pt-5">
          <p className="group-tips Monserrat">Group Tips</p>

          <div className="path-text d-flex Monserrat pt-2">
            <img src={path} alt="path" className="path" />
            <p className="ml-3">Lorem ipsum dolor sit amet, consectetur</p>
          </div>

          <div className="path-text d-flex Monserrat pt-2">
            <img src={path} alt="path" className="path" />
            <p className="ml-3">Lorem ipsum dolor sit amet, consectetur</p>
          </div>

          <div className="path-text d-flex Monserrat pt-2">
            <img src={path} alt="path" className="path" />
            <p className="ml-3">Lorem ipsum dolor sit amet, consectetur</p>
          </div>

          <div className="path-text d-flex Monserrat pt-2">
            <img src={path} alt="path" className="path" />
            <p className="ml-3">Lorem ipsum dolor sit amet, consectetur</p>
          </div>

          <div className="path-text d-flex Monserrat pt-2">
            <img src={path} alt="path" className="path" />
            <p className="ml-3">Lorem ipsum dolor sit amet, consectetur</p>
          </div>
        </div>
      </div>
      <div className=" col-sm-12 col-md-7">
        <div className="group-link ml-auto">
          <img src={Alt5} alt="Alt5" className="Alt5 p-4" />
          <div className="d-flex justify-content-between px-4">
            <p className="goto font-weight-bold">Go to Group</p>
            <img src={Vector30} alt="Vector" className="Vector m-3" />
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default Group;
