import React from "react";
import Card from "../components/Card";
import Item from "../components/Item";
import Image from "../assets/images/image 4.png";

const Module = () => (
  <div className="module">
    <div>
      <div className="col-md-8">
        <Card>
          <div className="h3 text muted font-weight-bolder Montserrat pt-3 px-4">
            Introduction to Product Design
            <h5 className="mt-5 font-weight-bold Montserrat">
              Course Overview
            </h5>
            <p className="design mt-2 font-weight-light Montserrat text-justify">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
              reprehenderit in voluptate velit esse cillum dolore eu fugiat
              nulla pariatur
            </p>
            <h5 className="mt-5 objective Montserrat">Objectives</h5>
            <p className="design mt-2 font-weight-light Nunito">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua.
            </p>
            <h5 className="mt-5 objective Montserrat">Module Guide</h5>
            <p className="design mt-2 font-weight-normal Montserrat text-justify">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
              reprehenderit in voluptate velit esse cillum dolore eu fugiat
              nulla pariatur.Lorem ipsum dolor sit amet, consectetur adipiscing
              elit,   sed do eiusmod tempor incididunt ut labore et dolore magna
              aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
              laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
              dolor in reprehenderit in voluptate velit esse cillum dolore eu
              fugiat nulla pariatur. Lorem ipsum dolor sit amet, consectetur
              adipiscing elit, sed do eiusmod tempor incididunt ut labore et
              dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
              exercitation ullamco laboris nisi ut aliquip ex ea commodo
              consequat. Duis aute irure dolor in reprehenderit in voluptate
              velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum
              dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
              incididunt ut labore et dolore magna aliqua. Ut enim ad minim
              veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
              ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
              voluptate velit esse cillum dolore eu fugiat nulla pariatur. Lorem
              ipsum dolor sit amet, consectetur adipiscing elit
              <a className="Montserrat design ml-5" href="_/_">
              (See More)
            </a>
            </p>
            
            <div className="border mt-5 mb-5"></div>
            <div className="video-container">
              <div className="video">
                <iframe
                  width={"100%"}
                  height={"400"}
                  src={"https://www.youtube.com/embed/PNACvjldl3I"}
                  frameBorder="0"
                  allow="autoplay; encrypted-media"
                  allowFullScreen
                  title={"video"}
                />
              </div>
            </div>
            </div>
        
        </Card>
      </div>
    </div>

    <div className="col-md-4">
      <Card className="px-4">
        <div className="week Montserrat">Weekly Task</div>
        <div className="border mt-3 mb-3"></div>

        <h5 className="week_design Monserrat pb-2">
          Design a Landing Page on Figma
        </h5>
        <p className="Montserrat text-justify">
          Lorem Ut fugiat elit pariatur commodo do commodo laborum. Aliquip
          laboris nisi minim magna nisi est. Occaecat ex mollit minim aliquip
          labore ex fugiat incididunt incididunt ea est mollit pariatur minim.
          Laboris veniam ea cupidatat est veniam eu excepteur irure Lorem.
        </p>

        <div className="week_hint Montserrat mt-5 font-weight-bold pb-2"> Hints</div>

        <p className="week_hint Montserrat text-justify">
          Lorem Ut fugiat elit pariatur commodo do commodo laborum. Aliquip
          laboris nisi minim magna nisi est.
        </p>
        <div className="border mt-3 mb-3"></div>

        <div className="week Montserrat mt-3">Task Submission</div>

        <form className="">
          <Item
            placeholder="http://localhost:3000/app/home/module"
            type="text"
            name="name"
          />
          <label class="col-form-label Montserat" for="url">
            Paste Uploaded Task Link
          </label>
        </form>

        <button
          id="submitButton"
          type="submit"
          className="btn btn-danger btn-lg btn-block py-3 mt-4">
        
          Submit
        </button>
      </Card>
    </div>

    <div className="col-md-4">
      <Card>
        <div className="week Montserrat mb-0">Material Library</div>
        <div className="border mt-3 mb-3"></div>

        <p className="week_access Montserrat mb-0">Access ebooks...etc</p>

        <Card className="mt-0 mb-0 pl-0">
          <div className="d-flex">
            <img className="img-fluid" src={Image} />
            <a href="_/_" className="week_access Montserrat my-auto ml-3">
              Access e-booksLaboris veniam ea estLaboris
            </a>
          </div>
        </Card>

        <Card className="mt-0 mb-0 pl-0">
          <div className="d-flex">
            <img className="img-fluid" src={Image} />
            <a href="_/_" className="week_access Montserrat my-auto ml-3">
              Access e-booksLaboris veniam ea estLaboris
            </a>
          </div>
        </Card>

        <Card className="mt-0 mb-0 pl-0">
          <div className="d-flex">
            <img className="img-fluid" src={Image} />
            <a href="_/_" className="week_access Montserrat my-auto ml-3">
              Access e-booksLaboris veniam ea estLaboris
            </a>
          </div>
        </Card>

        <Card className="mt-0 mb-0 pl-0">
          <div className="d-flex">
            <img className="img-fluid" src={Image} />
            <a href="_/_" className="week_access Montserrat my-auto ml-3">
              Access e-booksLaboris veniam ea estLaboris
            </a>
          </div>
        </Card>

        <Card className="mt-0 mb-0 pl-0">
          <div className="d-flex">
            <img className="img-fluid" src={Image} />
            <a href="_/_" className="week_access Montserrat my-auto ml-2">
              Access e-booksLaboris veniam ea estLaboris
            </a>
          </div>
        </Card>
      </Card>
    </div>
  </div>
);

export default Module;
