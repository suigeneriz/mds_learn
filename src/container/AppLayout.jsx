import React, { useState } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Sidebar from "../components/Sidebar";
import routes from "../routes";
import BreadCrumb from "../components/BreadCrumb";

const AppLayout = ({ location }) => {
  const [toggle, setToggle] = useState(false);

  const getRoutes = () => {
    let routeList = [];
      let childrenSwitch = [];
      routes.map((route, index) => {
      routeList.push(
        <Route
          key={`${index}-${route.route}`}
          exact={route.exact}
          path={`/app${route.route}`}
          component={route.component}
        />
      );
      if (route.children) {
        route.children.map((rou, i) =>
          childrenSwitch.push(
            <Route
              key={`${index}-${route.route}-${i}`}
              path={`/app${route.route}${rou.route}`}
              component={rou.component}
              exact={true}
            />
          )
        );
      }
    });
    routeList.push(<Switch>{childrenSwitch}</Switch>);
    return routeList;
  };

  return (
    <div className="app-layout">
      <Sidebar
        location={location}
        sidebarOpen={toggle}
        toggleSidebar={setToggle}
      />
      <div
        className={`overlay ${toggle ? "open" : ""}`}
        onClick={() => setToggle(false)}
      ></div>
      <div className="main">
        <BreadCrumb
          location={location}
          toggleSidebar={setToggle}
          sidebarOpen={toggle}
        />
        <Switch>
          {getRoutes()}
          <Redirect to="/app/home" />
        </Switch>
      </div>
    </div>
  );
};

export default AppLayout;
