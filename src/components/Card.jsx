import React from "react";

const Card = ({ children, className, onClick, as }) => {
  if (as === "a") {
    return (
      <a className={`card ${className}`} href={onClick}>
        {children}
      </a>
    );
  } else {
    return (
      <div className={`card ${className}`} onClick={onClick}>
        {children}
      </div>
    );
  }
};

Card.defaultProps = {
  onClick: () => {}
};

export default Card;
