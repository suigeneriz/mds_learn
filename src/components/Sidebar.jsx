import React from "react";
import routes from "../routes";
import { history } from "../index";

import Icons from "../assets/images/Icons";
import ProfileIcon from '../assets/images/profile.jpg'

const Sidebar = ({ location, sidebarOpen, toggleSidebar }) => {
  const activeRoute = routeName =>
    location.pathname.indexOf(routeName) > -1 ? "active" : "";

  const navigateTo = route => {
    history.push(route);
    toggleSidebar(false);
  };

  return (
    <div className={`sidebar ${sidebarOpen ? "open" : ""}`}>
      <div className="logo">
        <Icons.Logo />
      </div>
      <div className="sidebar-links">
        <ul>
          {routes.map((route, index) => {
            const Icon = Icons[route.name];
            return (
              <li
                key={`${index}-${route.route}`}
                className={`${activeRoute(route.route)}`}
                onClick={() => navigateTo(`/app${route.route}`)}
              >
                <span className="icon">
                  {Icon ? (
                    <Icon
                      fill={activeRoute(route.route) ? "#FF3C3C" : "#d3d3d3"}
                    />
                  ) : null}
                </span>
                <span className="link">{route.name}</span>
              </li>
            );
          })}
        </ul>
      </div>
      <div className="profile">
        <img src={ProfileIcon} alt="Choice React" />
        <div className="details">
          <p className='heading' >Choice React</p>
          <p>heybabby@gmail.com</p>
        </div>
        <span className="menu">
          <Icons.MenuUp />
        </span>
      </div>
    </div>
  );
};

export default Sidebar;
