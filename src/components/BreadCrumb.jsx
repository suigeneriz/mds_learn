import React from "react";
import routes from "../routes";
import Icon from "../assets/images/Icons";
import Vector from "../assets/images/Vector.png";

const BreadCrumb = ({ location, toggleSidebar, sidebarOpen }) => {
  const getBrandText = () => {
    let urlLocation = location.pathname.split("/");
    let brandName = { __html: "Brand" };
    let parentRoute = "";
    let currentRoute = {};
    if (urlLocation.length && urlLocation[2]) {
      let parentLocation = routes.filter(
        route => route.route === `/${urlLocation[2]}`
      )[0];
      currentRoute = parentLocation;
      if (parentLocation) {
        // brandName.__html = `<a href=/app${parentLocation.route} class='no-decoration' > ${urlLocation[3] ? 'Back to ' : ''} ${parentLocation.name}</a>`;
        brandName.__html = urlLocation[3]
          ? `<a href=/app${parentLocation.route} class='no-decoration' > Back to ${parentLocation.name} 
          <img src="../assets/images/Vector.png" >
           </a>`
           
          :`<span>${parentLocation.name}</span>`;
        parentRoute = parentLocation.route;
      }
    }
    if (
      urlLocation.length &&
      urlLocation[3] &&
      currentRoute &&
      currentRoute.children
    ) {
      let childLocation = currentRoute.children.filter(
        child => child.route === `/${urlLocation[3]}`
      )[0];
      if (childLocation) {
        brandName.__html += `<a href=/app${parentRoute}${childLocation.route} class='red' >
       
        ${childLocation.name}</a>`;
      }
    }
    return brandName;
  };
  return (
    <div className="breadcrumb">
      <div className="hambuger" onClick={() => toggleSidebar(!sidebarOpen)}>
        <Icon.Project fill="#FF3C3C" />
      </div>
      <span dangerouslySetInnerHTML={getBrandText()}></span>
    </div>
  );
};

export default BreadCrumb;
