import React from "react";

const Item = props => {
  return (
    <div className="">
      {/* col-form-label is the class name of css content inside index.css, and is
      use for styling you code */}
      <label className="item-label col-form-label">{props.label}</label>
      <input
        id="name"
        className="form-control"
        name={props.name}
        placeholder={props.placeholder}
        type={props.type}
        for={props.for}
      />
    </div>
  );
};

export default Item;
